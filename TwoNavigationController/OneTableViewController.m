//
//  OneTableViewController.m
//  TwoNavigationController
//
//  Created by mac on 14/12/11.
//  Copyright (c) 2014年 AliliWl. All rights reserved.
//

#import "OneTableViewController.h"

@interface OneTableViewController ()
@property (nonatomic,strong) NSMutableArray *staks;
@end

@implementation OneTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.staks = [@[@"asadad",@"fgdgdg",@"Alili",@"Alili"] mutableCopy];
    
//    NSLog(@"%@",self.myDelegate);
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"======%@",self.myDelegate);
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.staks.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell;
    if ([[self.staks objectAtIndex:indexPath.row] isEqualToString:@"Alili"]) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"twoCell" forIndexPath:indexPath];
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
    }
    UILabel *label = (UILabel *)[cell viewWithTag:1];
    label.text = [self.staks objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //获取indexPath
//    NSIndexPath *indexPath = [tableView indexPathForCell:sender];
    if ([[self.staks objectAtIndex:indexPath.row] isEqualToString:@"Alili"]) {
        NSLog(@"%ld",indexPath.row);
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Tips" message:[NSString stringWithFormat:@"Alili===%ld",indexPath.row] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alertView show];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //获取将要跳转到的ViewController
    UIViewController *destination = segue.destinationViewController;
//    //获取即将从屏幕上移除的ViewController
//    UIViewController *source = segue.sourceViewController;
 
//    Key-Value Coding,KVC:通过这种方式间接使用对象的取值方法和赋值方法
    if ([destination respondsToSelector:@selector(setDelegate:)])
    {
        [destination setValue:self forKey:@"delegate"];
    }
    [destination setValue:@"Hello World" forKey:@"myKey"];
    
}


@end
