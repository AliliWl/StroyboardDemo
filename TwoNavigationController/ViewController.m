//
//  ViewController.m
//  TwoNavigationController
//
//  Created by mac on 14/12/11.
//  Copyright (c) 2014年 AliliWl. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"%@",self.myKey);
}

- (void)viewWillDisappear:(BOOL)animated
{
    //在此回传值
    [self.delegate setValue:@"back" forKey:@"myDelegate"];
}

@end
